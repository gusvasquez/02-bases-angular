import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchGifsResponse, Gif } from '../interface/gifs.interface';

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  private apiKey: string = 'wkjP6XzGxc9j3wa8VQRzB2dWaerPWPYp';
  public servicioUrl: string = 'https://api.giphy.com/v1/gifs';
  private _historial: string[] = [];
  public resultados: Gif[] =[];

  constructor(private _http: HttpClient) {
    //if(localStorage.getItem('historial')){
      //this._historial = JSON.parse(localStorage.getItem('historial')!);
    //}
    // convertimos el json a su tipo original
    this._historial = JSON.parse(localStorage.getItem('historial')!) || [];
    this.resultados = JSON.parse(localStorage.getItem('images')!) || [];
  }


  get historial() {
    return [...this._historial];
  }

  buscarGifs(query: string) {

    query = query.trim().toLocaleLowerCase();

    // el metodo include del arreglo sirve para que nos guarde mismos datos al arreglo de historial, si no incluye ningun dato en el buscador entonces que lo guarde.
    if (!this._historial.includes(query)) {
      // guardar los datos en el arreglo desde  el comienzo de el.
      this._historial.unshift(query);
      // guardar solo 10 datos en el arreglo empezando desde la posicion 0 hasta la 10.
      this._historial = this._historial.splice(0, 10);
      // guardamos en el local Storage, convertimos el json en string
      localStorage.setItem('historial',JSON.stringify(this._historial));
    }
    // parametros para la peticion get, se añaden con set
    const params = new HttpParams().set('api_key', this.apiKey).set('limit', '10').set('q', query);

    this._http.get<SearchGifsResponse>(`${this.servicioUrl}/search?`, { params: params})
      .subscribe(response => {
        console.log(response.data);
        this.resultados = response.data;
        localStorage.setItem('images', JSON.stringify(response.data));
      });

  }
}
